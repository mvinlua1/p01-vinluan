CS580G Spring 2017  
Marvin Vinluan (mvinlua1)  
Project 01 - Hello World  
22 January 2017  

Contributors:  
Marvin Vinluan

Outside references:  
http://codewithchris.com/xcode-tutorial-user-interaction/

Git log:

commit f0174b9da39a5ed0b6584ebf8e51682c5e1e635b
Author: Marvin Vinluan <mvinlua1@binghamton.edu>
Date:   Sun Jan 22 18:49:51 2017 -0500

    Added working code

commit f7967b06adf980a1bb1cf014c36c7eafee7d38e5
Author: Marvin Vinluan <mvinlua1@binghamton.edu>
Date:   Sat Jan 21 15:00:02 2017 -0500

    trying to commit xcode proj again

commit 3ddf51fc28aff293769a63a5ca56c11a7799d87b
Author: Marvin Vinluan <mvinlua1@binghamton.edu>
Date:   Sat Jan 21 14:45:35 2017 -0500

    adding base xcode files

commit e129f2510f8119c599cd159358cc1d490381a5dd
Author: Marvin Vinluan <mvinlua1@binghamton.edu>
Date:   Thu Jan 19 11:17:34 2017 -0500

    Initial commit with contributors
