//
//  ViewController.h
//  CS580G-P01
//
//  Created by Marvin Vinluan on 1/21/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *helloLabel;
@property (strong, nonatomic) IBOutlet UIButton *helloButton;

@end

