//
//  ViewController.m
//  CS580G-P01
//
//  Created by Marvin Vinluan on 1/21/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize helloLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)handleClick:(id)sender {
    [helloLabel setText:@"Marvin Vinluan"];
}

@end
